/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 03/01/2021
 * Date class, date object with a month, day, and year
 */
package com.company;

import java.io.Serializable;

public class Date implements Comparable, Cloneable, Serializable {
    private int month;
    private int day;
    private int year;

    /**
     * default date constructor
     *
     * @pre: none
     * @post: default values assigned to instance variables
     */
    public Date() {
        this.month = 1;
        this.day = 1;
        this.year = 2021;
    }

    /**
     * parameterized date constructor
     *
     * @pre: month should be between 1-12, day should be between 1-31,
     * year should be between 2014-2024
     * @post: month, day, and year are assigned values
     * <p>
     * has dol argument
     */
    public Date(int month, int day, int year) {
        if (month < 1 || month > 12) {
            System.out.println("invalid month value, month defaulted to 1");
            this.month = 1;
        } else {
            this.month = month;
        }
        if (day < 1 || day > 31) {
            System.out.println("invalid day value, day defaulted to 1");
            this.day = 1;
        } else {
            this.day = day;
        }
        if (year < 2014 || year > 2024) {
            System.out.println("invalid year value, year defaulted to 2021");
            this.year = 2021;
        } else {
            this.year = year;
        }

    }

    /**
     * date copy constructor
     *
     * @pre: object other cannot be null
     * @post: assigns month, day, and year values from other
     */
//    public Date(Date other) {
//        if (other == null) {
//            System.out.println("null date, assigning default value");
//            this.month = 1;
//            this.day = 1;
//            this.year = 2021;
//        } else {
//            this.month = other.month;
//            this.day = other.day;
//            this.year = other.year;
//        }
//    }

    /**
     * month getter
     *
     * @pre: none
     * @post: returns month
     */
    public int getMonth() {
        return month;
    }

    /**
     * month setter
     *
     * @pre: month should be between 1-12
     * @post: assigns value to instance variable month
     */
    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            System.out.println("invalid month value, month defaulted to 1");
            this.month = 1;
        } else {
            this.month = month;
        }
    }

    /**
     * year getter
     *
     * @pre: none
     * @post: returns year
     */
    public int getYear() {
        return year;
    }

    /**
     * year setter
     *
     * @pre: year should be between 2014-2024
     * @post: assigns value to instance variable year
     */
    public void setYear(int year) {
        if (year < 2014 || year > 2024) {
            System.out.println("invalid year value, year defaulted to 2021");
            this.year = 2021;
        } else {
            this.year = year;
        }
    }

    /**
     * day getter
     *
     * @pre: none
     * @post: returns day
     */
    public int getDay() {
        return day;
    }

    /**
     * day setter
     *
     * @pre: day should be between 1-31
     * @post: assigns value to instance variable day
     */
    public void setDay(int day) {
        if (day < 1 || day > 31) {
            System.out.println("invalid day value, day defaulted to 1");
            this.day = 1;
        } else {
            this.day = day;
        }
    }

    /**
     * equals method
     *
     * @pre: passed object cannot be null
     * @post: returns true if date objects are equal
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        Date that = (Date) o;
        return this.month == that.month && this.day == that.day &&
                this.year == that.year;
    }

    /**
     * toString Override
     *
     * @pre: none
     * @post: return date in string format
     */
    @Override
    public String toString() {
        return this.month + "/" + this.day + "/" + this.year;
    }

    /**
     * compareTo method override for date class
     *
     * @pre object passed should be a money object
     * @post return 1 if this is larger than other, -1 if less, and
     * 0 if equal
     */
    // this compareTo method is implemented backwards
    // to have the earlier dates pushed to the top
    @Override
    public int compareTo(Object o) {
        if (o.getClass() != getClass()) {
            System.out.println("not a date object");
            return -111;
        }
        Date other = (Date) o;
        if (this.year == other.year && this.month == other.month &&
                this.day == other.day) {
            return 0;
        } else if (this.year > other.year) {
            return 1;
        } else if (this.year == other.year && this.month > other.month) {
            return 1;
        } else if(this.year == other.year && this.month == other.month &&
                this.day > other.day){
            return 1;
        } else {
            return -1;
        }

    }

    // instead of type casting could have typed interface Comparable<Date>
//    public int compareTo(Date other) {
//        if (this.year == other.year && this.month == other.month &&
//                this.day == other.day) {
//            return 0;
//        } else if (this.year == other.year && this.month < other.month) {
//            return -1;
//        } else if (this.year == other.year && this.month == other.month &&
//                this.day < other.day) {
//            return -1;
//        } else {
//            return 1;
//        }
//
//    }

    /**
     * date clone method Override
     *
     * @pre none
     * @post return a deep copy of the date object
     */
    @Override
    public Date clone() throws CloneNotSupportedException{
        Date d = (Date) super.clone();
        d.setMonth(this.month);
        d.setDay(this.day);
        d.setYear(this.year);
        return d;

    }
}
