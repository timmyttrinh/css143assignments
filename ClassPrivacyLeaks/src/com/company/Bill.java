/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 03/01/2021
 * Bill class, bill object with instance variables, amount, dueDate, paidDate,
 * and originator
 */
package com.company;

import java.io.Serializable;
import java.util.Locale;

public class Bill implements Comparable, Cloneable, Serializable {
    private Money amount;
    private Date dueDate;
    private Date paidDate;
    private String originator;
    //The internal paid date should be on or earlier than the due date.

    /**
     * bill default constructor
     *
     * @pre: none
     * @post: assigns default instance variables
     */
    public Bill() {
        this.amount = new Money();
        this.dueDate = new Date();
        this.originator = "originator";
        this.paidDate = null;
    }

    /**
     * bill constructor
     *
     * @pre: none
     * @post: assigns instance variables with copy constructors
     */
    public Bill(Money amount, Date dueDate, String originator) {
        try {
            this.amount = amount.clone();
            this.dueDate = dueDate.clone();
            this.originator = originator;
            this.paidDate = null;
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * bill copy constructor
     *
     * @pre: other cannot be null
     * @post: assigns instance variables using other bill object
     */
//    public Bill(Bill other) {
//        try {
//            if (other == null) {
//                System.out.println("null object, assigning default values");
//                this.amount = new Money();
//                this.dueDate = new Date();
//                this.originator = "originator";
//                this.paidDate = null;
//            } else if (!other.isPaid()) {
//                this.amount = other.amount.clone();
//                this.dueDate = other.dueDate.clone();
//                this.originator = other.originator;
//                this.paidDate = null;
//            } else {
//                this.amount = other.amount.clone();
//                this.dueDate = other.dueDate.clone();
//                this.originator = other.originator;
//                this.paidDate = other.paidDate.clone();
//            }
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }

    /**
     * getDueDate method
     *
     * @pre: none
     * @post: returns a copy of date object dueDate
     */
    public Date getDueDate() {
        Date retVal = new Date();
        try {
            retVal = this.dueDate.clone();
        } catch (Exception e) {
            System.out.println(e);
        }
        return retVal;
    }

    /**
     * getOriginator method
     *
     * @pre: none
     * @post: returns originator
     */
    public String getOriginator() {
        return this.originator;
    }

    /**
     * isPaid method
     *
     * @pre: none
     * @post: returns true if date object paidDate is not null
     */
    public boolean isPaid() {
        return this.paidDate != null;
    }

    /**
     * setPaid method
     *
     * @pre: date object onDay cannot be after date object dueDate
     * @post: assigns copy of date object onDay
     */
    public void setPaid(Date onDay) {
        try {
            if (onDay.getYear() > this.dueDate.getYear()) {
                System.out.println("cannot set paid date past the due date, " +
                        "paidDate remains null");
            } else if (onDay.getYear() == this.dueDate.getYear() &&
                    onDay.getMonth() > this.dueDate.getMonth()) {
                System.out.println("cannot set paid date past the due date, " +
                        "paidDate remains null");
            } else if (onDay.getYear() == this.dueDate.getYear() &&
                    onDay.getMonth() == this.dueDate.getMonth() &&
                    onDay.getDay() > this.dueDate.getDay()) {
                System.out.println("cannot set paid date past the due date, " +
                        "paidDate remains null");
            } else {
                this.paidDate = onDay.clone();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * setUnpaid method
     *
     * @pre: paidDate cannot already be null
     * @post: set paidDate to null
     */
    public void setUnpaid() {
        if (this.paidDate == null) {
            System.out.println("cannot set unpaid, already unpaid");
        } else {
            this.paidDate = null;
        }
    }

    /**
     * setDueDate method
     *
     * @pre: due date cannot be before paid date
     * @post: assign new due date
     */
    public void setDueDate(Date nextDate) {
        try {
            if (!this.isPaid()) {
                this.dueDate = nextDate.clone();
            } else if (nextDate.getYear() < this.paidDate.getYear()) {
                System.out.println("cannot set due date before the paid date, " +
                        "dueDate remains the same");
            } else if (nextDate.getYear() == this.paidDate.getYear() &&
                    nextDate.getMonth() < this.paidDate.getMonth()) {
                System.out.println("cannot set due date before the paid date, " +
                        "dueDate remains the same");
            } else if (nextDate.getYear() == this.paidDate.getYear() &&
                    nextDate.getMonth() == this.paidDate.getMonth() &&
                    nextDate.getDay() < this.paidDate.getDay()) {
                System.out.println("cannot set due date before the paid date, " +
                        "dueDate remains the same");
            } else {
                this.dueDate = nextDate.clone();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * setAmount method
     *
     * @pre: none
     * @post: assigns money object amount a new amount
     */
    public void setAmount(Money amount) {
        try {
            if (!isPaid()) {
                this.amount = amount.clone();
            } else {
                System.out.println("Cannot set amount, bill already paid");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * getAmount method
     *
     * @pre: none
     * @post: return copy of money object amount
     */
    public Money getAmount() {
        Money retVal = new Money();
        try {
            retVal = this.amount.clone();
        } catch (Exception e) {
            System.out.println(e);
        }
        return retVal;
    }

    /**
     * originator
     *
     * @pre: none
     * @post: assign originator a new String
     */
    public void setOriginator(String originator) {
        this.originator = originator;
    }

    /**
     * toString Override
     *
     * @pre: none
     * @post: returns a report of the bill
     */
    @Override
    public String toString() {
        if (isPaid()) {
            return " | Amount due: " + this.amount.toString() + " | Due date: " +
                    this.dueDate.toString() + " | Originator: " + this.originator +
                    " | Paid: " + isPaid() + " | Paid date: " + this.paidDate.toString();
        } else {
            return " | Amount due: " + this.amount.toString() + " | Due date: " +
                    this.dueDate.toString() + " | Originator: " + this.originator +
                    " | Paid: " + isPaid();
        }
    }

    /**
     * equals method
     *
     * @pre: other cannot be null
     * @post: returns true if bill object equals other
     */
    public boolean equals(Bill other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }

        if (this.isPaid() && other.isPaid()) {
            return this.amount.getMoney() == other.amount.getMoney() &&
                    this.dueDate.getMonth() == other.dueDate.getMonth() &&
                    this.dueDate.getDay() == other.dueDate.getDay() &&
                    this.dueDate.getYear() == other.dueDate.getYear() &&
                    this.paidDate.getMonth() == other.paidDate.getMonth() &&
                    this.paidDate.getDay() == other.paidDate.getDay() &&
                    this.paidDate.getYear() == other.paidDate.getYear() &&
                    this.originator.equals(other.originator);
        } else if (this.isPaid() && !other.isPaid()) {
            return false;

        } else if (!this.isPaid() && other.isPaid()) {
            return false;
        } else {
            return this.amount.getMoney() == other.amount.getMoney() &&
                    this.dueDate.getMonth() == other.dueDate.getMonth() &&
                    this.dueDate.getDay() == other.dueDate.getDay() &&
                    this.dueDate.getYear() == other.dueDate.getYear() &&
                    this.originator.equals(other.originator);
        }
    }

    // the compareTo method for the bill class will be implemented by
    // comparing dueDates, and when dueDates are equal, amount due is compared instead
    // and all paid bills are considered equal to one another

    /**
     * compareTo method override for bill class
     *
     * @pre object passed should be a bill object and not null
     * @post return 1 if this is larger than other, -1 if less, and
     * 0 if equal
     */
    @Override
    public int compareTo(Object o) {
        if (o.getClass() != getClass()) {
            System.out.println("not a bill object");
            return -111;
        }
        Bill other = (Bill) o;
        if (this.isPaid() && other.isPaid()) {
            return 0;
        } else if (this.isPaid() && !other.isPaid()) {
            return 1;
        } else if (!this.isPaid() && other.isPaid()) {
            return -1;
        }
        if (this.dueDate.compareTo((other.dueDate)) != 0) {
            return this.dueDate.compareTo(other.dueDate);
        } else {
            return this.amount.compareTo(other.amount);
        }
    }
     //instead of type casting could have typed interface Comparable<Bill>
//    public int compareTo(Bill other) {
//
//        if (this.isPaid() && other.isPaid()) {
//            return 0;
//        } else if (this.isPaid() && !other.isPaid()) {
//            return 1;
//        } else if (!this.isPaid() && other.isPaid()) {
//            return -1;
//        }
//        if (this.dueDate.compareTo(other.dueDate) == 0) {
//            if(this.amount.compareTo(other.amount) == 0){
//                return 0;
//            } else if(this.amount.compareTo(other.amount) == -1){
//                return -1;
//            } else {
//                return 1;
//            }
//        } else if (this.dueDate.compareTo(other.dueDate) == -1){
//            return 1;
//        } else {
//            return -1;
//        }
//
//    }

    /**
     * bill clone method Override
     *
     * @pre none
     * @post return a deep copy of the bill object by making use of the
     * date and money clone methods
     */
    public Bill clone() throws CloneNotSupportedException {
        Bill b = (Bill) super.clone();
        b.setAmount(this.amount.clone());
        b.setDueDate(this.dueDate.clone());
        if (paidDate != null) {
            b.setPaid(this.paidDate.clone());
        }
        b.setOriginator(this.originator);
        return b;
    }

}
