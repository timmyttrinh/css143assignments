/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 03/01/2021
 * ExpenseAccount class, extends an arraylist that is of type Bill
 */
package com.company;

public class ExpenseAccount extends ArrayList<Bill> {

    /**
     * sort method, sorts arraylist of bills using the
     * compareTo method provided by Bill class
     *
     * @pre none
     * @post sort the array list
     */
    // this sort will sort with paid bills being the "largest" and largest amount of money due
    // and earliest due dates being the "smallest"
    // the list of bills would have the earliest due dates on top, and the largest bills
    // on top
    public void sort() {
        try {

            for (int i = 0; i < size(); i++) {
                for (int j = 0; j < size(); j++) {
                    Bill temp1 = get(i);
                    Bill temp2 = get(j);
                    if (temp1.compareTo(temp2) < 0) {
                        this.list[i] = temp2;
                        this.list[j] = temp1;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * display method
     *
     * @pre none
     * @post prints out the list of bills
     */
    public void display() {
        try {
            double total = 0;
            for (int i = 0; i < size(); i++) {
                System.out.println(get(i));
                if (!get(i).isPaid()) {
                    total += get(i).getAmount().getMoney();
                }
            }
            System.out.println(" | Total amount due: " + total);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
