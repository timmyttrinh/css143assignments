/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * Linked List class
 */
package com.company;

public class List {
    public class Node {
        public Object data;
        public Node next;

        /**
         * no arg constructor
         *
         * @pre none
         * @post assign data null, next null
         */
        public Node() {
            this.data = null;
            this.next = null;
        }

        /**
         * parameterized constructor
         *
         * @pre none
         * @post assign data and link values passed
         */
        public Node(Object data, Node link) {
            this.data = data;
            this.next = link;
        }

        @Override
        public String toString(){
            return this.next.data + "";
        }


    }

    Node head;

    public List() {
        this.head = null;
    }

    /**
     * insert method
     *
     * @throws LinkedListException thrown when invalid index used
     * @pre index cannot be negative or larger than size of list
     * @post insert a node at the desired index
     */
    public void insert(Object newData, int index) throws LinkedListException {
        if (index > size() || index < 0) {
            throw new LinkedListException("cannot insert at index " + index);
        } else if (index == 0) {
            if (this.head == null) {
                this.head = new Node(newData, null);
            } else {
                this.head = new Node(newData, this.head);
            }
        } else {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < index; i++) {
                pre = cur;
                cur = cur.next;
            }
            pre.next = new Node(newData, cur);
        }

    }

    /**
     * remove method
     *
     * @throws LinkedListException thrown when invalid index used
     * @pre index cannot be negative or larger than size of list
     * @post remove node at desired index, return object at that index
     */
    public Object remove(int index) throws LinkedListException {
        Object removed;
        if (index >= size() || index < 0) {
            throw new LinkedListException("an object does not exist at index " +
                    index);
        } else if (this.head == null) {
            throw new LinkedListException("list is empty");
        } else {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < index; i++) {
                pre = cur;
                cur = cur.next;

            }
            removed = new Node(pre, cur);
            if (index != 0) {
                pre.next = cur.next;
            } else {
                this.head = cur.next;
            }
        }
        return removed;
    }

    /**
     * append method
     *
     * @pre none
     * @post adds newData to the end of the list
     */
    public void append(Object newData) {
        if (this.head != null) {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < size(); i++) {
                pre = cur;
                cur = cur.next;

            }
            pre.next = new Node(newData, null);
        } else {
            this.head = new Node(newData, null);
        }
    }

    /**
     * delete method
     *
     * @pre none
     * @post removes object at index, no return value
     */
    public void delete(int index) throws LinkedListException {
        if (index >= size() || index < 0) {
            throw new LinkedListException("an object does not exist at index " +
                    index);
        }
        Node pre = null;
        Node cur = this.head;
        for (int i = 0; i < index; i++) {
            pre = cur;
            cur = cur.next;

        }
        if (index != 0) {
            pre.next = cur.next;
        } else {
            this.head = cur.next;
        }
    }

    /**
     * size method
     *
     * @pre none
     * @post loop through and count all nodes in the list
     */
    public int size() {
        int count = 0;
        Node cur = this.head;
        while (cur != null) {
            cur = cur.next;
            count++;
        }
        return count;
    }

    /**
     * toString method Override
     *
     * @pre none
     * @post return string representation of a list
     */
    @Override
    public String toString() {
        String retVal = "";
        Node cur = this.head;
        while (cur != null) {
            retVal += cur.data + ", ";
            cur = cur.next;
        }
        return retVal;
    }

    /**
     * isEmpty method
     *
     * @pre none
     * @post return true if list is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * indexOf method
     *
     * @pre none
     * @post return index of target object, return -1 if not found
     */
    public int indexOf(Object target) {
        int index = 0;
        Node cur = this.head;
        while (cur != null) {
            if (cur.data == target) {
                return index;
            }
            index++;
            cur = cur.next;
        }
        return -1;
    }

    /**
     * equals method
     *
     * @pre none
     * @post return true if lists are equal to one another
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        List o = (List) other;
        Node n1 = this.head;
        Node n2 = o.head;
        if (o.size() == size()) {
            while (n1 != null && n2 != null) {
                if (n1.data != n2.data) {
                    return false;
                }
                n1 = n1.next;
                n2 = n2.next;
            }
            return true;
        }
        return false;
    }

}
