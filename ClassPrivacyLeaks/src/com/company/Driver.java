
/**
 * @author Timmy Trinh, borrowed from Rob Nash
 * @version 1.0.0
 * @since 03/01/2021
 * Driver, tests Money object, Date objects, and bill objects
 */
package com.company;

public class Driver {

    /**
     * main driver function
     *
     * @pre: none
     * @post: exercises the methods in Bill, Money, and Date
     */
    public static void main(String[] args) {
        try {
            Date d = new Date(7, 27, 2015);
            Date d1 = d.clone();
            System.out.println(d);
            System.out.println(d1);
            System.out.println(d.equals(d1));

            Money m = new Money(10, 75);
            Money m2 = new Money(23, 99);
            System.out.println(m.getMoney());
            System.out.println(m.toString());
            m.add(10, 47);
            System.out.println(m.getMoney());
            m.add(m2);
            System.out.println(m.getMoney());
            //Construct some money
            Money money1 = new Money(10);
            Money money2 = money1.clone();
            money1.setMoney(30, 50);
            //TODO: do more functional exercises with the money class


            System.out.println("Money objects output:");
            System.out.println(money1);
            System.out.println(money2);


            //Construct some bills
            Money amount = new Money(20);
            Date dueDate = new Date(4, 30, 2017);

            Bill bill1 = new Bill(amount, dueDate, "The phone company");
            Bill bill4 = bill1.clone();
            bill1.setPaid(new Date(4, 30, 2018));
            bill1.setPaid(new Date(4, 30, 2017));

            Bill bill2 = bill1.clone();
            bill2.setDueDate(new Date(9, 30, 2017));
            amount.setMoney(31, 99);
            dueDate.setDay(29);
            Bill bill3 = new Bill(amount, dueDate, "The record company");
            bill2.setUnpaid();
            System.out.println("Bill objects output:");
            bill4.setOriginator("haha");
            System.out.println(bill1);
            System.out.println(bill1.getAmount());
            System.out.println(bill1.getDueDate());
            System.out.println(bill1.getOriginator());
            bill2.setAmount(new Money(500, 99));
            System.out.println(bill2);
            System.out.println(bill2.getDueDate());
            System.out.println(bill2.getAmount());
            System.out.println(bill3);
            System.out.println(bill4);
            Bill bill5 = bill4.clone();
            System.out.println(bill5);

            System.out.println(bill1.equals(bill2));
            System.out.println(bill1.equals(bill3));
            System.out.println(bill1.equals(bill4));
            System.out.println(bill5.equals(bill4));

//        Date date = new Date();
//        Date date2 = new Date();
//
//        date2.setYear(2020);
//        System.out.println(date.compareTo(date2));

            Date db = new Date(1, 1, 2021);
            Date dc = new Date(1, 1, 2020);
            Bill b = new Bill();
            Bill b2 = new Bill(new Money(1, 20), db, "org");
            Bill b3 = new Bill(new Money(1, 10), dc, "org");
            System.out.println(b);
            System.out.println(b2);
            System.out.println(b3);
            System.out.println(b.compareTo(b2));
            System.out.println(b.compareTo(b3));
            b.setAmount(new Money(100, 25));
            System.out.println(b);
            System.out.println(b.compareTo(b2));
            Bill b4 = b.clone();
            System.out.println(b.compareTo(b4));

            Date d23 = new Date();
            Date d24 = d23.clone();
            System.out.println(d23);
            System.out.println(d24);

            ExpenseAccount e = new ExpenseAccount();
            bill4.setPaid(new Date(1, 23, 2014));
            Bill b24 = new Bill(new Money(300, 10),
                    new Date(9, 30, 2015), "Company");
            Bill b26 = b24.clone();
            b26.setAmount(new Money(500,15));
            Bill b27 = b24.clone();
            b27.setAmount(new Money(10,15));
            e.insert(b, 0);
            e.insert(bill1, 1);
            e.insert(b24, 2);
            e.insert(bill3, 3);
            e.insert(bill4, 4);
            e.insert(bill2, 5);
            e.insert(b26,6);
            e.insert(b27,7);
            bill3.setDueDate(new Date(1, 1, 2016));
            //System.out.println(e.remove(4));
//            System.out.println(b);
//            System.out.println(bill1);
//            System.out.println(bill2);
//            System.out.println(bill3);
//            System.out.println(bill4);
//            System.out.println(b24);
//            System.out.println(bill2.compareTo(bill3));
//            System.out.println(bill2.compareTo(b));
            //System.out.println(b24.compareTo());
            System.out.println();

            e.sort();
            e.display();

//            ExpenseAccount e2 = new ExpenseAccount();
//
//            e2.insert(new Bill(), 0);
//            e2.insert(new Bill(), 0);
//            e2.insert(new Bill(), 5);
//            e2.insert(new Bill(), 4);
//            System.out.println(e2);
//            System.out.println(e2.remove(0));

            //e2.sort(); //can't sort empty expense account
           // e2.display(); //can't display anything empty

//            ArrayList<Bill> a = new ArrayList<>();
//
//            a.append(new Bill());
//            a.append(new Bill());
//            a.append(new Bill());
//            System.out.println(a);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

