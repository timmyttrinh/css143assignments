/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 03/01/2021
 * Money class, has dollars and cents
 */
package com.company;

import java.io.Serializable;

public class Money implements Comparable, Cloneable, Serializable {
    private int dol;
    private int cent;

    /**
     * default money constructor
     *
     * @pre: none
     * @post: default values assigned to instance variables
     * <p>
     * has no arguments
     */
    public Money() {
        this.dol = 0;
        this.cent = 0;
    }

    /**
     * parameterized money constructor
     *
     * @pre: dol is not negative
     * @post: dol is assigned to dol, cent is defaulted to 0
     * <p>
     * has dol argument
     */
    public Money(int dol) {
        if (dol < 0) {
            System.out.println("invalid value, dol defaulted to 0");
            this.dol = 0;
        } else {
            this.dol = dol;
        }
        this.cent = 0;
    }

    /**
     * overloaded parameterized money constructor
     *
     * @pre: dol and cent cannot be negative, cent should be no more than 99
     * @post: dol and cent are assigned
     * <p>
     * has dol and cent arguments
     */
    public Money(int dol, int cent) {
        if (dol < 0) {
            System.out.println("invalid value, dol defaulted to 0");
            this.dol = 0;
        } else {
            this.dol = dol;
        }

        if (cent < 0 || cent > 99) {
            System.out.println("invalid value, cent defaulted to 0");
            this.cent = 0;
        } else {
            this.cent = cent;
        }
    }

    /**
     * money copy constructor
     *
     * @pre: other cannot be null
     * @post: dol and cent are assigned values from other object
     * <p>
     * has other Money object as argument
     */
//    public Money(Money other) {
//        if (other == null) {
//            System.out.println("null, setting default values");
//            this.dol = 0;
//            this.cent = 0;
//        } else {
//            this.dol = other.dol;
//            this.cent = other.cent;
//        }
//    }

    /**
     * dol getter
     *
     * @pre: none
     * @post: returns dol
     */
    public int getDol() {
        return this.dol;
    }

    /**
     * dol setter
     *
     * @pre: dol cannot be negative
     * @post: dol assigned value dol
     */
    public void setDol(int dol) {
        if (dol < 0) {
            System.out.println("invalid value, dol defaulted to 0");
            this.dol = 0;
        } else {
            this.dol = dol;
        }
    }

    /**
     * cent getter
     *
     * @pre: none
     * @post: returns cent
     */
    public int getCent() {
        return this.cent;
    }

    /**
     * cent setter
     *
     * @pre: cent should be between 0-99
     * @post: cent assigned value cent
     */
    public void setCent(int cent) {
        if (cent < 0 || cent > 99) {
            System.out.println("invalid value, cent defaulted to 0");
            this.cent = 0;
        } else {
            this.cent = cent;
        }
    }

    /**
     * getMoney method
     *
     * @pre: none
     * @post: returns dol and cent as a combined double
     */
    public double getMoney() {
        double dolD = this.dol;
        double centD = this.cent;
        centD = centD / 100;
        return dolD + centD;
    }

    /**
     * setMoney method
     *
     * @pre: dol and cent are not negative, cent is between 0-99
     * @post: uses setters to assign values dol and cent
     */
    public void setMoney(int dol, int cent) {
        setDol(dol);
        setCent(cent);
    }

    /**
     * add method
     *
     * @pre: dol cannot be negative
     * @post: dol will be added to instance variable dol
     */
    public void add(int dol) {
        if (dol < 0) {
            System.out.println("invalid value, adding 0 to dol");
        } else {
            this.dol = this.dol + dol;
        }
    }

    /**
     * add method
     *
     * @pre: dol and cent cannot be negative, cent should be between 0-99
     * @post: dol and cent will be added to instance variable dol and cent
     */
    public void add(int dol, int cents) {
        if (dol < 0) {
            System.out.println("invalid value, adding 0 to dol");
        } else {
            this.dol = this.dol + dol;
        }
        if (cents < 0 || cents > 99) {
            System.out.println("invalid value, adding 0 to cent");
        } else {
            int val = this.cent + cents;
            if (val > 99) {
                this.dol++;
                this.cent = val - 100;
            } else {
                this.cent = this.cent + cents;
            }
        }
    }

    /**
     * add method
     *
     * @pre: other cannot be null
     * @post: other will be added
     */
    public void add(Money other) {
        if (other == null) {
            System.out.println("null object, adding 0");
        } else {
            this.dol = this.dol + other.dol;
            int val = this.cent + other.cent;
            if (val > 99) {
                this.dol++;
                this.cent = val - 100;
            } else {
                this.cent = this.cent + other.cent;
            }
        }

    }

    /**
     * equals method
     *
     * @pre: passed object cannot be null
     * @post: returns true if money objects are equal
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        Money that = (Money) o;
        return this.dol == that.dol && this.cent == that.cent;
    }

    /**
     * toString Override
     *
     * @pre: none
     * @post: return dol and cent in string format
     */
    @Override
    public String toString() {
        return this.dol + "." + this.cent;
    }

    /**
     * compareTo method override for money class
     *
     * @pre object passed should be a money object
     * @post return 1 if this is larger than other, -1 if less, and
     * 0 if equal
     */
    // this compareTo is implemented in a backwards manner to have
    // the more expensive bills pushed at the top of the list in the
    // event the dates compared are equal
    @Override
    public int compareTo(Object o) {
        if (o.getClass() != getClass()) {
            System.out.println("not a money object");
            return -111;
        }
        Money other = (Money) o;
        if (this.getMoney() == other.getMoney()) {
            return 0;
        } else if (this.getMoney() < other.getMoney()) {
            return 1;
        } else {
            return -1;
        }
    }

    // instead of type casting could have typed interface Comparable<Money>
//    public int compareTo(Money other) {
//        if(this.getMoney() == other.getMoney()){
//            return 0;
//        } else if(this.getMoney() < other.getMoney()) {
//            return -1;
//        } else {
//            return 1;
//        }
//    }

    /**
     * money clone method Override
     *
     * @pre none
     * @post return a deep copy of the money object
     */
    @Override
    public Money clone() throws CloneNotSupportedException {
        Money m = (Money) super.clone();
        m.setMoney(this.dol, this.cent);
        return m;
    }
}
