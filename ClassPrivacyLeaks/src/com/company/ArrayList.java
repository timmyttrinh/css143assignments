package com.company;

/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 03/01/2021
 * ArrayList class, holds objects in an array
 */

public class ArrayList<T> {
    public Object[] list = new Object[1];

    /**
     * insert
     *
     * @PRE: index cannot be negative
     * @POST: the desired object gets inserted at the indicated index
     */
    public void insert(T obj, int index) {
        // checks for negative index
        if (index < 0) {
            System.out.println("bad input");

            // resize array if bigger than array length
        } else if (index >= this.list.length) {
            Object[] newList = new Object[index + 1];
            for (int i = 0; i < this.list.length; i++) {
                newList[i] = this.list[i];
            }
            this.list = newList;
            this.list[index] = obj;

            // shifts array if element contains object
        } else if (this.list[index] != null) {
            Object[] newList = new Object[this.list.length + 1];
            for (int i = 0; i < index; i++) {
                newList[i] = this.list[i];
            }
            for (int i = index; i < this.list.length; i++) {
                newList[i + 1] = this.list[i];
            }
            newList[index] = obj;
            this.list = newList;

        } else {
            this.list[index] = obj;
        }


    }

    /**
     * remove
     *
     * @PRE: index cannot be negative
     * @POST: removes object at indicated index and shifts elements to left
     */
    public T remove(int index) throws Exception {
        Object removedObj;
        // checks for negative index
        if (index < 0) {
            throw new Exception("negative index");
            // checks if index is bigger than array size
        } else if (index >= this.list.length) {
            throw new Exception("index is out of bounds");
        } else {
            removedObj = this.list[index];
            Object[] newList = new Object[this.list.length - 1];
            for (int i = 0; i < index; i++) {
                newList[i] = this.list[i];
            }
            for (int i = index; i < this.list.length - 1; i++) {
                newList[i] = this.list[i + 1];
            }
            this.list = newList;
        }

        return (T) removedObj;
    }

    /**
     * size
     *
     * @PRE: none
     * @POST: return length of the array list
     */
    public int size() {
        return this.list.length;
    }

    /**
     * toString Override
     *
     * @PRE: none
     * @POST: returns elements in array separated by a comma
     */
    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < this.list.length; i++) {
            s += this.list[i] + " | ";
        }
        return s;
    }

    /**
     * isEmpty
     *
     * @PRE: none
     * @POST: return true if the array is empty, return false if it is not
     */
    public boolean isEmpty() {
        for (int i = 0; i < this.list.length; i++) {
            if (list[i] != null) {
                return false;
            }
        }
        return true;
    }

    /**
     * indexOf
     *
     * @PRE: assume each object is unique
     * @POST: returns the index of object, returns -1 if not found
     */
    public int indexOf(T obj) {
        int index = 0;
        for (int i = 0; i < this.list.length; i++) {
            if (this.list[i] == obj) {
                return index;
            }
            index++;
        }
        return -1;
    }

    /**
     * equals
     *
     * @PRE: should only pass other arrayLists
     * @POST: return true if arrayLists are equals, false if not
     */
    public boolean equals(ArrayList<T> otherList) {
        boolean retBool = true;
        if (otherList == null) {
            return false;
        }
        if (otherList == this) {
            return true;
        }
        if (this.list.length != otherList.size()) {
            return false;
        } else {
            for (int i = 0; i < this.list.length; i++) {
                if (this.list[i] != otherList.list[i]) {
                    retBool = false;
                }
            }
        }
        return retBool;
    }

    /**
     * get
     *
     * @PRE: index cannot be negative
     * @POST: return object in array at index
     */
    public T get(int index) throws Exception {
        if (index < 0) {
            throw new Exception("Negative index");
        } else {
            return (T) this.list[index];
        }
    }

    /**
     * append method
     *
     * @pre none
     * @post insert at the end of the list
     */
    public void append(T o) {
        if (this.list[0] == null) {
            insert(o, 0);
        } else {
            insert(o, this.list.length);
        }
    }

    public double doubleIn(T in) {
        return in.doubleValue() * 2.0;
    }


    public T returnThis(T in) {
        return in;
    }
}

