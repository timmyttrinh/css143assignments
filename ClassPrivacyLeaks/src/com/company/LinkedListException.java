/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * LinkedListException class, extends Exception
 */
package com.company;

public class LinkedListException extends Exception {

    /**
     * LinkedListException class
     *
     * @pre none
     * @post call super class
     */
    public LinkedListException() {
        super();
    }

    /**
     * LinkedListException
     *
     * @pre none
     * @post call super class using string msg
     */
    public LinkedListException(String msg) {
        super(msg);
    }
}

