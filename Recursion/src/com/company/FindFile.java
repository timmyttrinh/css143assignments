/**
 * @author Timmy Trinh
 * @since 02/15/2021
 * FindFile class
 */
package com.company;

import java.io.File;

public class FindFile {
    private int count;
    private int MAX_NUMBER_OF_FILES_TO_FIND;
    private String[] fileLocations = new String[0];
    File file;

    /**
     * FindFile constructor
     *
     * @pre maxFiles cannot be negative
     * @post assign MAX_NUMBER_OF_FILES_TO_FIND value maxFiles
     *
     * @param maxFiles manually assign maximum files are able to be found
     */
    public FindFile(int maxFiles) {
        if (maxFiles < 0) {
            throw new IllegalArgumentException("negative maximum file input");
        } else {
            this.MAX_NUMBER_OF_FILES_TO_FIND = maxFiles;
        }
    }

    /**
     * directorySearch method
     * <p>
     * when a target file is found, that the location of that file
     * gets recorded in array of strings
     *
     * @param target  string of target
     * @param dirName string of directory name
     * @throws Exception thrown when the number of files found exceed MAX_NUMBER_OF_FILES_TO_FIND
     * @pre none
     * @post the target files get recorded in an array
     */
    public void directorySearch(String target, String dirName) throws Exception {
        this.file = new File(dirName);
        File[] list = this.file.listFiles();
        if (this.count > this.MAX_NUMBER_OF_FILES_TO_FIND) {
            throw new Exception("files found have exceeded MAX_NUMBER_OF_FILES_TO_FIND");
        }
        if (list != null) {
            for (File f : list) {
                if (target.equals(f.getName())) {
                    expandArray();
                    this.fileLocations[this.count] = target + " is located at " + f.getAbsolutePath();
                    this.count++;
                }
                if (f.isDirectory()) {
                    directorySearch(target, f.getAbsolutePath());
                }

            }

        } else {
            throw new IllegalArgumentException();
        }

    }

    /**
     * count getter
     *
     * @pre none
     * @post return count
     */
    public int getCount() {
        return this.count;
    }

    /**
     * files found array getter
     * <p>
     * gets the array that contains the locations that files are found
     *
     * @pre none
     * @post return the array of found file locations, with the size limit being
     * MAX_NUMBER_OF_FILES_TO_FIND
     */
    public String[] getFiles() {
        int length;
        if (this.MAX_NUMBER_OF_FILES_TO_FIND > this.fileLocations.length) {
            length = this.fileLocations.length;
        } else {
            length = this.MAX_NUMBER_OF_FILES_TO_FIND;
        }
        String[] copy = new String[length];

        for (int i = 0; i < length; i++) {
            copy[i] = fileLocations[i];
        }
        return copy;
    }

    /**
     * expandArray helper method
     * <p>
     * expands array by size one
     *
     * @pre none
     * @post use temp array newArr which gets size +1 of fileLocations length
     */
    private void expandArray() {
        String[] newArr = new String[this.fileLocations.length + 1];
        for (int i = 0; i < this.fileLocations.length; i++) {
            newArr[i] = fileLocations[i];
        }
        this.fileLocations = newArr;
    }
}
