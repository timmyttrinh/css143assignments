/**
 * @author Timmy Trinh
 * @since 02/15/2021
 * LinearSearch class, extends SearchAlgorithm
 */
package com.company;

public class BinarySearch extends SearchAlgorithm {
    private int low;
    private int mid;
    private int high;
    private int lex;

    /**
     * binary search method
     * <p>
     * iterative binary search of a sorted list of strings
     *
     * @param words      string array of words
     * @param wordToFind target word being searched
     * @throws ItemNotFoundException is thrown when target word not found
     * @pre string array words cannot be null
     * @post returns index of wordToFind
     */
    @Override
    public int search(String[] words, String wordToFind) throws ItemNotFoundException {
        int low = 0;
        int high = words.length - 1;
        int mid;
        while (low <= high) {
            mid = (low + high) / 2;
            int lex = (wordToFind.compareTo(words[mid]));// lexicographical comparison
            if (lex == 0) {
                incrementCount();
                return mid;
            }
            if (lex > 0) {
                incrementCount();
                incrementCount();
                low = mid + 1;
            } else {
                incrementCount();
                incrementCount();
                incrementCount();
                high = mid - 1;
            }

        }
        return -1; // never found
    }

    /**
     * binary recSearch method
     * <p>
     * recursive binary search of a sorted list of strings
     *
     * @param words      string array of words
     * @param wordToFind string target of search
     * @throws ItemNotFoundException is thrown when failing to find target word
     * @pre word array cannot be null
     * @post returns the index of the wordToFind
     */
    @Override
    public int recSearch(String[] words, String wordToFind) throws ItemNotFoundException {
        if (this.high == 0) {
            this.high = words.length - 1;
        }

        if (this.low <= this.high) {
            this.mid = (this.low + this.high) / 2;
            this.lex = (wordToFind.compareTo(words[mid]));

            if (lex == 0) {
                incrementCount();
                return this.mid;
            }
            if (lex > 0) {
                incrementCount();
                incrementCount();
                this.low = this.mid + 1;
                return recSearch(words, wordToFind);
            } else {
                incrementCount();
                incrementCount();
                incrementCount();
                this.high = this.mid - 1;
                return recSearch(words, wordToFind);
            }

        }
        return -1;
    }
}