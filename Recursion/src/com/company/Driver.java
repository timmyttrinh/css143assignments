/**
 * @author Timmy Trinh
 * @since 02/15/2021
 * FindFile driver,
 */
package com.company;

public class Driver {
    public static void main(String[] args) {

        // to be found
        String targetFile = "Main.jav";
        // path to search
        String pathToSearch = "C:\\Users\\timmy\\IdeaProjects";
        // number of files that can be found limit
        int MAX_NUMBER_OF_FILES_TO_FIND = 3;

        FindFile finder = new FindFile(MAX_NUMBER_OF_FILES_TO_FIND);

        try {
            finder.directorySearch(targetFile, pathToSearch);
        } catch (Exception e) {
            System.err.println(e);
        }

//        System.out.println(finder.getCount());
        // print array of found files and their locations
        String[] files = finder.getFiles();
        for (int i = 0; i < files.length; i++) {
            System.out.println(files[i]);
        }
    }


}
