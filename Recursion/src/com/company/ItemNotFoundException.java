/**
 * @author Timmy Trinh
 * @since 02/15/2021
 * ItemNotFoundException class, inherits from exception class
 */
package com.company;

public class ItemNotFoundException extends Exception {

    /**
     * no arg ItemNotFoundException constructor
     *
     * @pre none
     * @post calls super class with an error message
     */
    public ItemNotFoundException() {
        super("target not found");
    }

    /**
     * ItemNotFoundException constructor, takes string arg
     *
     * @param msg manually set a message
     * @pre none
     * @post calls super class using the String msg
     */
    public ItemNotFoundException(String msg) {
        super(msg);
    }
}
