/**
 * @author Timmy Trinh
 * @since 02/15/2021
 * LinearSearch class, extends SearchAlgorithm
 */
package com.company;

public class LinearSearch extends SearchAlgorithm {
    private int index;

    /**
     * search method
     * <p>
     * iteratively searches array of strings for target using for loop
     *
     * @param words      string array of words
     * @param wordToFind target word to be found
     * @throws ItemNotFoundException thrown when target word not found
     * @pre string array words cannot be null
     * @post return the index of wordToFind
     */
    @Override
    public int search(String[] words, String wordToFind) throws ItemNotFoundException {
        for (int i = 0; i < words.length; i++) {
            if (wordToFind.equals(words[i])) {
                return i;
            }
            incrementCount();
        }
        return -1;
    }

    /**
     * linear recSearch method
     * <p>
     * uses recursion to linearly search an array of strings for it's target
     *
     * @param words      string array of words
     * @param wordToFind word to be found
     * @throws ItemNotFoundException thrown when target word not found
     * @pre word string array cannot be null
     * @post this method will crash because of a StackOverflowError
     * this method will never reach it's base case
     */
    @Override
    public int recSearch(String[] words, String wordToFind) throws ItemNotFoundException {
        if (this.index == words.length) {
            return -1;
        }
        if (wordToFind.equals(words[this.index])) {
            return this.index;
        }
        this.index++;
        incrementCount();
        return recSearch(words, wordToFind);
    }


}
