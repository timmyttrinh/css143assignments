/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * FractionCounter class, each FractionCounter object holds a fraction object and the count of it
 */
package com.company;

public class FractionCounter {
    private Fraction uniqueFraction;
    private int counter;

    /**
     * default constructor
     */
    public FractionCounter(Fraction newFraction) {
        this.uniqueFraction = newFraction;
        this.counter = 1;
    }

    /**
     * compareAndIncrement
     *
     * compares two fractions
     *
     * @PRE: none
     * @POST: returns true and increments by one if fraction equal to newFraction
     */
   public boolean compareAndIncrement(Fraction newFraction) {
        if (this.uniqueFraction.equals(newFraction)) {
            this.counter++;
            return true;
        } else {
            return false;
        }

    }

    /**
     * toString Override
     *
     * @PRE: none
     * @POST: return unique fraction and count of it
     */
    @Override
    public String toString(){
       return this.uniqueFraction.getNumerator() + "/" +
               this. uniqueFraction.getDenominator() + " has a count of " +
               this.counter;
    }

}
