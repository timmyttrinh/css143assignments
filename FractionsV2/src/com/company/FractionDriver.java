/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * FractionDriver
 */
package com.company;

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class FractionDriver {

    public static void main(String[] args) {
        ArrayList<FractionCounter> fractionCounterList = new ArrayList<>();

        File inputFile = new File("fractions.txt");
        try {
            Scanner fractionReader = new Scanner(inputFile);

            while (fractionReader.hasNextLine()) {
                String fraction = fractionReader.nextLine();
                String[] splitFraction = fraction.split("/");
                Fraction theFraction = new Fraction(Integer.parseInt(splitFraction[0]),
                        Integer.parseInt(splitFraction[1]));

                if (fractionCounterList.isEmpty()) {
                    FractionCounter counter = new FractionCounter(theFraction);
                    fractionCounterList.add(counter);
                } else {
                    boolean needsNewCounter = true;
                    for (int i = 0; i < fractionCounterList.size(); i++) {
                        if (fractionCounterList.get(i).compareAndIncrement(theFraction)) {
                            needsNewCounter = false;
                        }
                    }
                    if (needsNewCounter) {
                        FractionCounter counter = new FractionCounter(theFraction);
                        fractionCounterList.add(counter);
                    }
                }
            }
            fractionReader.close();
        } catch (Exception e) {
            System.out.println("File not found!");
        }
        for (int i = 0; i < fractionCounterList.size(); i++) {
            System.out.println(fractionCounterList.get(i).toString());
        }

    }
}
