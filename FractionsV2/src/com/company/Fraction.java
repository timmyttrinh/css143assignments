/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * Fraction class, each fraction object has a numerator and denominator
 */
package com.company;

public class Fraction {
    private int numerator;
    private int denominator;

    /**
     * default constructor
     */
    public Fraction() {
        this.numerator = 1;
        this.denominator = 1;
    }

    /**
     * parameterized constructor
     */
    public Fraction(int newNum, int newDen) {
        if (newDen < 0) {
            newNum = newNum - (newNum * 2);
            newDen = newDen - (newDen * 2);
        }
        setNumerator(newNum / gcd(newNum,newDen));
        setDenominator(newDen / gcd(newNum,newDen));

    }

    /**
     * gcd
     * <p>
     * calculates greatest common divisor of the numerator and the denominator using the Euclidean algorithm
     *
     * @PRE: none
     * @POST: return greatest common divisor
     */
    private static int gcd(int numGcd, int denGcd) {
        numGcd = Math.abs(numGcd);
        denGcd = Math.abs(denGcd);
        if (numGcd == 0) {
            return denGcd;
        }
        while (denGcd != 0) {
            if (numGcd > denGcd) {
                numGcd = numGcd - denGcd;
            } else {
                denGcd = denGcd - numGcd;
            }
        }
        return numGcd;
    }

    /**
     * equals method override
     *
     * compares two objects
     *
     * @PRE: none
     * @POST: return true if this object equals other object
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        Fraction o = (Fraction) other;
        if (this.getNumerator() == o.getNumerator() &&
                this.getDenominator() == o.getDenominator()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * toString Override
     *
     * string of fraction
     *
     * @PRE: none
     * @POST: return string representation of the fraction
     */
    @Override
    public String toString() {
        return this.numerator + "/" + this.denominator;
    }

    /**
     * getNumerator
     * <p>
     * returns numerator
     */
    public int getNumerator() {
        return numerator;
    }

    /**
     * getDenominator
     * <p>
     * returns denominator
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * setNumerator
     *
     * numerator set to value newNum
     */
    public void setNumerator(int newNum) {
        this.numerator = newNum;
    }

    /**
     * setDenominator
     *
     * denominator set to value newDen
     */
    public void setDenominator(int newDen) {
        if (newDen == 0) {
            System.out.println("cannot set denominator to 0, fraction defaulted to 1/1");
            this.denominator = 1;
        } else {
            this.denominator = newDen;
        }
    }
}
