/**
 * @author Timmy Trinh,
 * @since 02/06/2021
 * Rectangle class, inherits from parent class shape, representation of a rectangle
 */
package com.company;

import java.awt.*;

public class Rectangle extends Shape {
    private double width;
    private double height;

    /**
     * Rectangle constructor
     *
     * @pre: none
     * @post: call super class
     */
    public Rectangle(int a, int b) {
        super(a, b);
    }

    /**
     * getArea method Override
     *
     * @pre: none
     * @post: returns the area of the rectangle using width and height
     */
    @Override
    public double getArea() {
        return this.width * this.height;
    }

    /**
     * draw method Override for rectangle
     *
     * @pre: none
     * @post: draws a rectangle
     */
    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        setHeight(Math.random() * 50);
        setWidth(Math.random() * 100);
        g2d.drawRect(this.getX(), this.getY(), (int) this.width, (int) this.height);
    }

    /**
     * width getter
     *
     * @pre: none
     * @post: return width of the rectangle
     */
    public double getWidth() {
        return width;
    }

    /**
     * width setter
     *
     * @pre: none
     * @post: assign width value w
     */
    public void setWidth(double w) {
        this.width = w;
    }

    /**
     * height getter
     *
     * @pre: none
     * @post: return height of the rectangle
     */
    public double getHeight() {
        return height;
    }

    /**
     * height setter
     *
     * @pre: none
     * @post: assign height with value h
     */
    public void setHeight(double h) {
        this.height = h;
    }
}
