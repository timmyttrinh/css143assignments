/**
 * @author Timmy Trinh,
 * @since 02/06/2021
 * Smiley class, inherits from parent class shape, representation of a smiley face
 */
package com.company;

import java.awt.*;

public class Smiley extends Shape {
    private int scale;

    /**
     * smiley constructor
     *
     * @pre: none
     * @post: calls super class
     */
    public Smiley(int a, int b) {
        super(a, b);
    }

    /**
     * draw method Override for smiley
     *
     * @pre: none
     * @post: draws a smiley face
     */
    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        setScale((int) (Math.random() * 50));
        g2d.fillOval(getX() + this.scale, getY(), this.scale / 6, this.scale / 6);
        g2d.fillOval(getX(), getY(), this.scale / 6, this.scale / 6);
        g2d.drawArc(getX(), getY(), this.scale, this.scale, 0, -180);

    }

    /**
     * scale getter
     *
     * @pre: none
     * @post: return scale
     */
    public int getScale() {
        return scale;
    }

    /**
     * scale setter
     *
     * @pre: none
     * @post: assign scale to value s
     */
    public void setScale(int s) {
        this.scale = s;
    }

}
