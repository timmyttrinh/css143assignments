/**
 * @author Timmy Trinh, borrowed from Rob Nash
 * @since 02/06/2021
 * Shape class, parent class
 */
package com.company;

import java.awt.*;

/* Class Shape
 *
 * By Rob Nash (with minor edits by David Nixon)
 *
 * This is the superclass in a hierarchy of shapes that you have to construct
 */

// the superclass in our inheritance hierarchy
// all "common" features, functions and data should go here
// for example, all shapes in Java2D have a x,y that declares their position
// and many of the shapes exposed have a width and a height (but not all, so we didn't put width and height here)
// note that this class is mostly empty, as there is no algorithm generic enough to guess an arbitrary shape's area
// (future subclasses must override getArea() to provide something reasonable)
// Also, the draw method is empty too, as we don't know what shape to draw here!
// (again, our subclasses will need to replace this method with one that actually draws things)


class Shape extends Object {
    private int x = 0;
    private int y = 0;

    /**
     * default shape constructor
     *
     * @pre: none
     * @post: assigns x and y the value 0
     */
    public Shape() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * shape constructor that takes a and b
     *
     * @pre: none
     * @post: assigns x and y values a and b
     */
    public Shape(int a, int b) {
        this.x = a;
        this.y = b;
    }

    /**
     * getArea method
     *
     * @pre: none
     * @post: returns -1 because shape class has no area to get
     */
    public double getArea() {
        return -1;
    }

    /**
     * draw method
     *
     * @pre: none
     * @post: nothing, because shape class has nothing to draw
     */
    public void draw(Graphics g) {

    }

    /**
     * getX method
     *
     * @pre: none
     * @post: returns x
     */
    public int getX() {
        return this.x;
    }

    /**
     * getY method
     *
     * @pre: none
     * @post: returns y
     */
    public int getY() {
        return this.y;
    }

    /**
     * setX protected method, can only be access by subclasses
     *
     * @pre: none
     * @post: assigns x value newX
     */
    protected void setX(int newX) {
        this.x = newX;
    }

    /**
     * setY protected method, can only be access by subclasses
     *
     * @pre: none
     * @post: assigns y value newY
     */
    protected void setY(int newY) {
        this.y = newY;
    }
}
