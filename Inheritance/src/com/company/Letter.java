/**
 * @author Timmy Trinh,
 * @since 02/06/2021
 * Letter class, inherits from parent class shape, representation of a letter
 */
package com.company;

import java.awt.*;

public class Letter extends Shape {
    private String letter;

    /**
     * Letter constructor
     *
     * @pre: none
     * @post: call super class
     */
    public Letter(int a, int b) {
        super(a, b);
    }

    /**
     * draw method Override for letter
     *
     * @pre: none
     * @post: draws a letter
     */
    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        setLetter("T");
        g2d.drawString(this.letter, getX(), getY());
    }

    /**
     * letter getter
     *
     * @pre: none
     * @post: return letter
     */
    public String getLetter() {
        return letter;
    }

    /**
     * letter setter
     *
     * @pre: none
     * @post: assign letter with value l
     */
    public void setLetter(String l) {
        this.letter = l;
    }
}
