/**
 * @author Timmy Trinh,
 * @since 02/06/2021
 * Circle class, inherits from parent class shape, representation of a circle
 */

package com.company;

import java.awt.*;

public class Circle extends Shape {
    private double radius;

    /**
     * circle constructor
     *
     * @pre: none
     * @post: calls super class
     */
    public Circle(int a, int b) {
        super(a, b);
    }

    /**
     * Circle constructor, takes x, y, and r
     *
     * @pre: none
     * @post: assigns x, y, radius vales x, y, and r
     */
    public Circle(int x, int y, int r) {
        this.setX(x);
        this.setY(y);
        this.radius = r;

    }

    /**
     * getArea method Override
     *
     * @pre: none
     * @post: returns the area of the circle using area formula
     */
    @Override
    public double getArea() {
        return Math.PI * (this.radius * this.radius);
    }

    /**
     * draw method Override for circle
     *
     * @pre: none
     * @post: draws a circle
     */
    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        setRadius(Math.random() * 100);
        g2d.drawOval(this.getX(), this.getY(), (int) getRadius() * 2, (int) getRadius() * 2);
    }

    /**
     * getRadius method
     *
     * @pre: none
     * @post: returns radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * setRadius method
     *
     * @pre: none
     * @post: assign radius with value r
     */
    public void setRadius(double r) {
        this.radius = r;
    }
}
