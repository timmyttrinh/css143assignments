/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/07/2021
 */

/*
Questions:
1) It would be possible to use a scanner and if/switch statements to check for every possible fraction.
You can use one variable, count.
2) A 2d array could be used, or a 1d array can have numerators be odds and denominators be evens.
The data type for the array could be int or char.
3) This is when a class is using another class as an instance variable. Therefore, this
class is "composed" of other objects/classes. This is done by declaring another class
as a class instance variable.
4) Recursion can be used instead of Euclid's GCD algorithm.
 */
package com.company;

import java.io.File;
import java.util.Scanner;

public class FractionsV1 {
    public static int[] numerator = new int[100];
    public static int[] denominator = new int[100];

    public static void main(String[] args) {
        int count = 0;
        int[] num = new int[100];
        int[] den = new int[100];

        File inputFile = new File("fractions.txt");

        try {
            Scanner fractionReader = new Scanner(inputFile);

            //while loop traverses txt file and splits fractions
            while (fractionReader.hasNextLine()) {
                String fraction = fractionReader.nextLine();
                String[] splitFraction = fraction.split("/");
                num[count] = Integer.parseInt(splitFraction[0]); //converts to int
                den[count] = Integer.parseInt(splitFraction[1]); //converts to int

                //count fractions
                count++;

            }

            fractionReader.close();

        } catch (Exception e) {
            System.out.println("File not found!");
        }

        simplifyFraction(num, den, count);

//for(int i = 0; i < 31;i++) {
//    System.out.println(numerator[i] + "/" + denominator[i]);
//}
        countFractions();

    }

    /**
     * countFractions
     * <p>
     * count the number of each unique fractions
     *
     * @PRE: none
     * @POST: prints "fraction" has a count of "count" of unique fractions
     */
    public static void countFractions() {
        int count = 0;
        for (int i = 0; i < numerator.length; i++) {
            for (int j = 0; j < numerator.length; j++) {
                if (numerator[i] == 0 && denominator[i] == 0 && numerator[j] == 0 & denominator[j] == 0) {
                    break;
                } else if (numerator[i] == numerator[j] && denominator[i] == denominator[j]) {
                    count++;
                    if (i != j) {
                        numerator[j] = 0;
                        denominator[j] = 0;
                    }
                } else {
                    continue;
                }
            }
            if (numerator[i] != 0 && denominator[i] != 0) {
                System.out.println(numerator[i] + "/" + denominator[i] + " has a count of " + count);
            }
            count = 0;
        }
    }

    /**
     * simplifyFraction
     * <p>
     * simplifies fractions using greatest common divisor
     *
     * @PRE: should only pass array num and den
     * @POST: simplified fraction is placed in numerator and denominator arrays
     */
    public static void simplifyFraction(int[] numArray, int[] denArray, int fractionCount) {
        for (int i = 0; i < fractionCount; i++) {
            numerator[i] = numArray[i] / gcd(numArray[i], denArray[i]);
            denominator[i] = denArray[i] / gcd(numArray[i], denArray[i]);
        }
    }

    /**
     * gcd
     * <p>
     * calculates greatest common divisor of the numerator and the denominator using the Euclidean algorithm
     *
     * @PRE: none
     * @POST: return greatest common divisor
     */
    public static int gcd(int numGcd, int denGcd) {
        if (numGcd == 0) {
            return denGcd;
        }
        while (denGcd != 0) {
            if (numGcd > denGcd) {
                numGcd = numGcd - denGcd;
            } else {
                denGcd = denGcd - numGcd;
            }
        }
        return numGcd;
    }
}
