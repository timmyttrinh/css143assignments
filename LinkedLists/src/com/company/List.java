/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * Linked List class
 */
package com.company;

public class List {
    public class Node {
        public Object data;
        public Node next;

        /**
         * no arg constructor
         *
         * @pre none
         * @post assign data null, next null
         */
        public Node() {
            this.data = null;
            this.next = null;
        }

        /**
         * parameterized constructor
         *
         * @pre none
         * @post assign data and link values passed
         */
        public Node(Object data, Node link) {
            this.data = data;
            this.next = link;
        }

        @Override
        public String toString(){
            return this.next.data + "";
        }


    }

    Node head;

    public List() {
        this.head = null;
    }

    /**
     * insert method
     *
     * @throws LinkedListException thrown when invalid index used
     * @pre index cannot be negative or larger than size of list
     * @post insert a node at the desired index
     */
    public void insert(Object newData, int index) throws LinkedListException {
        if (index > size() || index < 0) {
            throw new LinkedListException("cannot insert at index " + index);
        } else if (index == 0) {
            if (this.head == null) {
                this.head = new Node(newData, null);
            } else {
                this.head = new Node(newData, this.head);
            }
        } else {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < index; i++) {
                pre = cur;
                cur = cur.next;
            }
            pre.next = new Node(newData, cur);
        }

    }

    /**
     * remove method
     *
     * @throws LinkedListException thrown when invalid index used
     * @pre index cannot be negative or larger than size of list
     * @post remove node at desired index, return object at that index
     */
    public Object remove(int index) throws LinkedListException {
        Object removed;
        if (index >= size() || index < 0) {
            throw new LinkedListException("an object does not exist at index " +
                    index);
        } else if (this.head == null) {
            throw new LinkedListException("list is empty");
        } else {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < index; i++) {
                pre = cur;
                cur = cur.next;

            }
            removed = new Node(pre, cur);
            if (index != 0) {
                pre.next = cur.next;
            } else {
                this.head = cur.next;
            }
        }
        return removed;
    }

    /**
     * append method
     *
     * @pre none
     * @post adds newData to the end of the list
     */
    public void append(Object newData) {
        if (this.head != null) {
            Node pre = null;
            Node cur = this.head;
            for (int i = 0; i < size(); i++) {
                pre = cur;
                cur = cur.next;

            }
            pre.next = new Node(newData, null);
        } else {
            this.head = new Node(newData, null);
        }
    }

    /**
     * delete method
     *
     * @pre none
     * @post removes object at index, no return value
     */
    public void delete(int index) throws LinkedListException {
        if (index >= size() || index < 0) {
            throw new LinkedListException("an object does not exist at index " +
                    index);
        }
        Node pre = null;
        Node cur = this.head;
        for (int i = 0; i < index; i++) {
            pre = cur;
            cur = cur.next;

        }
        if (index != 0) {
            pre.next = cur.next;
        } else {
            this.head = cur.next;
        }
    }

    /**
     * size method
     *
     * @pre none
     * @post loop through and count all nodes in the list
     */
    public int size() {
        int count = 0;
        Node cur = this.head;
        while (cur != null) {
            cur = cur.next;
            count++;
        }
        return count;
    }

    /**
     * toString method Override
     *
     * @pre none
     * @post return string representation of a list
     */
    @Override
    public String toString() {
        String retVal = "";
        Node cur = this.head;
        while (cur != null) {
            retVal += cur.data + ", ";
            cur = cur.next;
        }
        return retVal;
    }

    /**
     * isEmpty method
     *
     * @pre none
     * @post return true if list is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * indexOf method
     *
     * @pre none
     * @post return index of target object, return -1 if not found
     */
    public int indexOf(Object target) {
        int index = 0;
        Node cur = this.head;
        while (cur != null) {
            if (cur.data == target) {
                return index;
            }
            index++;
            cur = cur.next;
        }
        return -1;
    }

    /**
     * equals method
     *
     * @pre none
     * @post return true if lists are equal to one another
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        List o = (List) other;
        Node n1 = this.head;
        Node n2 = o.head;
        if (o.size() == size()) {
            while (n1 != null && n2 != null) {
                if (n1.data != n2.data) {
                    return false;
                }
                n1 = n1.next;
                n2 = n2.next;
            }
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        try {
            System.out.println("list tests");
            List l = new List();
            List l2 = new List();
            List l3 = new List();
            l.insert("0", 0);
            l.insert(1, 0);
            l.insert(2, 1);
            l.insert(3, 3);

            l2.insert("0", 0);
            l2.insert(1, 0);
            l2.insert(2, 1);
            l2.insert(3, 3);

            l.append("end");
            l2.append("end");
//            l.remove(99); attempting to remove at index past list size
//
//            l.delete(3);
            // l.delete(4);
            // l.insert(23,9); index larger than list size
            // l3.remove(0); empty list
            System.out.println(l.toString());
            System.out.println(l2.toString());
            System.out.println(l.size());
            System.out.println(l.isEmpty());
            System.out.println(l.equals(l2));
            System.out.println(l2.equals(l3));
            System.out.println(l.indexOf(2));
            System.out.println(l.remove(0));
            List empty = new List();
            List one = new List();
            List multiple = new List();

            one.append(5);
            multiple.append(10);
            multiple.append(20);
            multiple.append(30);

            System.out.println("Empty:" + empty);     // ( note the implicit call to toString()! )
            System.out.println("One:" + one);
            System.out.println("Multiple:" + multiple);

            one.delete(0);
            multiple.delete(1);
            System.out.println("One (upon delete):" + one);
            System.out.println("Multiple (upon delete):" + multiple);

            //one.insert(600, 1);
            multiple.insert(400, 2);
            System.out.println("One (on insert):" + one);
            System.out.println("Multiple(on insert):" + multiple);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
