/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * Linked List Stack class
 */
package com.company;

public class Stack extends List {

    /**
     * Stack constructor
     *
     * @pre none
     * @post calls super class constructor
     */
    public Stack() {
        super();
    }

    /**
     * push method
     *
     * @pre none
     * @post call super class insert method at index 0 (head)
     */
    public void push(Object next) throws LinkedListException {
        super.insert(next, 0);
    }

    /**
     * pop method
     *
     * @pre none
     * @post pops head of a list
     */
    public Object pop() throws LinkedListException {
        if (this.head == null) {
            throw new LinkedListException("empty list, cannot pop head");
        } else {
            return super.remove(0);
        }
    }

    /**
     * insert override
     *
     * @pre none
     * @post call push method, disregard index param
     */
    @Override
    public void insert(Object newData, int index) throws LinkedListException {
        push(newData);
    }

    /**
     * remove override
     *
     * @pre none
     * @post return object obtained from calling pop, disregard index
     */
    @Override
    public Object remove(int index) throws LinkedListException {
        return pop();
    }

    public static void main(String[] args) {
        try {
            //stack tests
            System.out.println("stack tests");
            Stack s = new Stack();
            Stack s2 = new Stack();
            s.push(0);
            s.push(1);
            System.out.println(s.toString());
            System.out.println(s.isEmpty());
            System.out.println(s2.isEmpty());
            s.pop();
            // s.remove(0);
            // s2.pop(); empty list
            System.out.println(s.toString());
            System.out.println(s2.size());

            Stack s3 = new Stack();

            s3.push(0);
            s3.push(1);
            s3.push(2);
            s3.push(3);
            System.out.println(s3.toString());
// objects get popped first if they were pushed last
            while (!s3.isEmpty()) {
                System.out.println(s3.pop());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
