/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * Driver, tests for List, Stack, and Queue
 */
package com.company;

public class Driver {

    public static void main(String[] args) {
        // write your code here
        try {
            System.out.println("list tests");
            List l = new List();
            List l2 = new List();
            List l3 = new List();
            l.insert("0", 0);
            l.insert(1, 0);
            l.insert(2, 1);
            l.insert(3, 3);

            l2.insert("0", 0);
            l2.insert(1, 0);
            l2.insert(2, 1);
            l2.insert(3, 3);

            l.append("end");
            l2.append("end");
//            l.remove(99);
//
//            l.delete(3);
            // l.delete(4);
            //l.insert(23,9);
            System.out.println(l.toString());
            System.out.println(l2.toString());
            System.out.println(l.size());
            System.out.println(l.isEmpty());
            System.out.println(l.equals(l2));
            System.out.println(l2.equals(l3));
            System.out.println(l.indexOf(2));

            List empty = new List();
            List one = new List();
            List multiple = new List();

            one.append(5);
            multiple.append(10);
            multiple.append(20);
            multiple.append(30);

            System.out.println("Empty:" + empty);     // ( note the implicit call to toString()! )
            System.out.println("One:" + one);
            System.out.println("Multiple:" + multiple);

            one.delete(0);
            multiple.delete(1);
            System.out.println("One (upon delete):" + one);
            System.out.println("Multiple (upon delete):" + multiple);

            //one.insert(600, 1);
            multiple.insert(400, 2);
            System.out.println("One (on insert):" + one);
            System.out.println("Multiple(on insert):" + multiple);

            //stack tests
            System.out.println();
            System.out.println("stack tests");
            Stack s = new Stack();
            Stack s2 = new Stack();
            s.push(0);
            s.push(1);
            System.out.println(s.toString());
            System.out.println(s.isEmpty());
            System.out.println(s2.isEmpty());
            s.pop();
            //s.remove(0);
            // s2.pop();
            System.out.println(s.toString());
            System.out.println(s2.size());

            Stack s3 = new Stack();

            s3.push(0);
            s3.push(1);
            s3.push(2);
            s3.push(3);

            while (!s3.isEmpty()) {
                System.out.println(s3.toString());
                s3.pop();
            }

            //queue tests
            System.out.println();
            System.out.println("queue tests");
            Queue q = new Queue();
            Queue q2 = new Queue();

            q.enqueue(0);
            q.enqueue(1);
            System.out.println(q.equals(q2));
            System.out.println(q.toString());
            q.dequeue();
            System.out.println(q.toString());
            q.enqueue(99);
            System.out.println(q.toString());
            q.dequeue();
            q.dequeue();
            System.out.println(q.equals(q2));
//            q.dequeue();

            Queue q3 = new Queue();

            q3.enqueue(0);
            q3.enqueue(1);
            q3.enqueue(2);
            q3.enqueue(3);

            while (!q3.isEmpty()) {
                System.out.println(q3.dequeue() + "");

            }
        } catch (Exception e) {
            System.out.println(e);
        }


    }

}
