/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/21/2021
 * Linked List Queue class
 */
package com.company;

public class Queue extends List {
    /**
     * Queue constructor
     *
     * @pre none
     * @post calls super class constructor
     */
    public Queue() {
        super();
    }

    /**
     * enqueue method
     *
     * @pre none
     * @post call super class insert method at index size (end of a list)
     */
    public void enqueue(Object next) throws LinkedListException {
        super.insert(next, super.size());
    }

    /**
     * dequeue method
     *
     * @pre none
     * @post dequeues head of a list
     */
    public Object dequeue() throws LinkedListException {
        if (this.head == null) {
            throw new LinkedListException("empty list, cannot dequeue head");
        } else {
            return super.remove(0);
        }
    }

    /**
     * insert override
     *
     * @pre none
     * @post call enqueue method, disregard index param
     */
    @Override
    public void insert(Object newData, int index) throws LinkedListException {
        enqueue(newData);
    }

    /**
     * remove override
     *
     * @pre none
     * @post return object obtained from calling dequeue, disregard index
     */
    @Override
    public Object remove(int index) throws LinkedListException {
        return dequeue();
    }

    public static void main(String[] args) {
        try {
            //queue tests
            System.out.println("queue tests");
            Queue q = new Queue();
            Queue q2 = new Queue();

            q.enqueue(0);
            q.enqueue(1);
            System.out.println(q.equals(q2));
            System.out.println(q.toString());
            q.dequeue();
            System.out.println(q.toString());
            q.enqueue(99);
            System.out.println(q.toString());
            q.dequeue();
            q.dequeue();
            System.out.println(q.equals(q2));
//            q.dequeue();

            Queue q3 = new Queue();

            q3.enqueue(0);
            q3.enqueue(1);
            q3.enqueue(2);
            q3.enqueue(3);
            System.out.println(q3.toString());
            // objects get dequeued first if they were queued first
            while (!q3.isEmpty()) {
                System.out.println(q3.dequeue());
            }

            Queue q4 = new Queue();
            q4.dequeue(); // attempt to dequeue empty queue
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
