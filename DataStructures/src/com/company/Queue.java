/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/23/2021
 * Queue class, array holds objects, first in, first out
 */
package com.company;

public class Queue {
    private Object[] list = new Object[0];

    /**
     * enqueue
     *
     * @PRE: none
     * @POST: array size increase by one, obj queued at the end of array
     */
    public void enqueue(Object obj) {

        Object[] newList = new Object[this.list.length + 1];
        for (int i = 0; i < this.list.length; i++) {
            newList[i] = this.list[i];
        }
        this.list = newList;
        this.list[this.list.length - 1] = obj;
    }

    /**
     * pop
     *
     * @PRE: none
     * @POST: returns dequeued object, and shifts array to the left
     */
    public Object dequeue() {
        if(isEmpty()){
            System.out.println("cannot dequeue an empty array");
            return -1;
        }
        Object dequeued = this.list[0];
        Object[] newList = new Object[this.list.length - 1];
        for (int i = 0; i < this.list.length - 1; i++) {
            newList[i] = this.list[i + 1];
        }
        this.list = newList;
        return dequeued;
    }
    /**
     * toString Override
     *
     * @PRE: none
     * @POST: returns elements in array separated by a comma
     */
    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < this.list.length; i++) {
            s += this.list[i] + ", ";
        }
        return s;
    }
    /**
     * isEmpty
     *
     * @PRE: none
     * @POST: return true if the array is empty, return false if it is not
     */
    public boolean isEmpty() {
        for (int i = 0; i < this.list.length; i++) {
            if (list[i] != null) {
                return false;
            }
        }
        return true;
    }
    /**
     * size
     *
     * @PRE: none
     * @POST: return length of the queue array
     */
    public int size() {
        return this.list.length;
    }

    /**
     * equals
     *
     * @PRE: should only pass other Queues
     * @POST: return true if Queues are equals, false if not
     */
    public boolean equals(Queue otherList) {
        boolean retBool = true;
        if (otherList == null) {
            return false;
        }
        if (otherList == this) {
            return true;
        }
        if (this.list.length != otherList.size()) {
            return false;
        } else {
            for (int i = 0; i < this.list.length; i++) {
                if (this.list[i] != otherList.list[i]) {
                    retBool = false;
                }
            }
        }
        return retBool;
    }
}

