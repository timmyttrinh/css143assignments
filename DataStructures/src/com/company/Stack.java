/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/23/2021
 * Stack class, array holds objects, last in, first out
 */
package com.company;

public class Stack {
    private Object[] list = new Object[0];

    /**
     * push
     *
     * @PRE: none
     * @POST: object obj pushes onto front of array,
     * array shifts to the right
     */
    public void push(Object obj) {
        Object[] newList = new Object[this.list.length + 1];
        newList[0] = obj;
        for (int i = 0; i < this.list.length; i++) {
            newList[i + 1] = this.list[i];
        }
        this.list = newList;
    }

    /**
     * pop
     *
     * @PRE: none
     * @POST: returns popped object, and shifts array to the left
     */
    public Object pop() {
        if(isEmpty()){
            System.out.println("cannot pop an empty array");
            return -1;
        }
        Object popped = this.list[0];
        Object[] newList = new Object[this.list.length - 1];
        for (int i = 0; i < this.list.length - 1; i++) {
            newList[i] = this.list[i + 1];
        }
        this.list = newList;
        return popped;
    }

    /**
     * toString Override
     *
     * @PRE: none
     * @POST: returns elements in array separated by a comma
     */
    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < this.list.length; i++) {
            s += this.list[i] + ", ";
        }
        return s;
    }

    /**
     * isEmpty
     *
     * @PRE: none
     * @POST: return true if the array is empty, return false if it is not
     */
    public boolean isEmpty() {
        for (int i = 0; i < this.list.length; i++) {
            if (list[i] != null) {
                return false;
            }
        }
        return true;
    }

    /**
     * size
     *
     * @PRE: none
     * @POST: return length of the stack array
     */
    public int size() {
        return this.list.length;
    }
    /**
     * equals
     *
     * @PRE: should only pass other Stacks
     * @POST: return true if Stacks are equals, false if not
     */
    public boolean equals(Stack otherList) {
        boolean retBool = true;
        if (otherList == null) {
            return false;
        }
        if (otherList == this) {
            return true;
        }
        if (this.list.length != otherList.size()) {
            return false;
        } else {
            for (int i = 0; i < this.list.length; i++) {
                if (this.list[i] != otherList.list[i]) {
                    retBool = false;
                }
            }
        }
        return retBool;
    }
}
