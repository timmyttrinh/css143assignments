package com.company;

/*
 *  Incomplete Driver for ArrayList(ObjectList), Stack and Queue
 *
 *
 */

public class ArrayBasedDataStructuresDriver {

    public static void main(String[] args) {
//        stackTests();
//        queueTests();
        arrayListTests();


    }

    private static void arrayListTests() {
//        //todo: make more tests here
        ArrayList a = new ArrayList();
//        ArrayList b = new ArrayList();
//        ArrayList c = new ArrayList();
//        System.out.println(a.equals(b));
//        System.out.println(a.isEmpty());
//        a.insert(null, 0);
//        System.out.println(a.isEmpty());
//        a.insert(1, 0);
//        a.insert(2, 0);
//        a.insert(3, 0);
//        a.insert(4, 0);
//        System.out.println(a.toString());
//        System.out.println(a.remove(1));
//        System.out.println(a.toString());
//        a.insert(5, 4);
//        System.out.println(a.toString());
//        a.insert(6, 4);
//        System.out.println(a.toString());
//        System.out.println(a.remove(3));
//        System.out.println(a.toString());
//        a.insert(27, 5);
//        System.out.println(a.toString());
//        System.out.println(a.remove(5));
//        System.out.println(a.toString());
//        System.out.println(a.isEmpty());
//
        a.insert('B', 0);
        a.insert('a', 0);
        a.insert('t', 1);
        a.insert('B', 0);
        a.insert('a', 0);
        a.insert('t', 1);
        a.insert('B', 0);
        a.insert('a', 0);
        a.insert('t', 1);
        a.insert('B', 0);
        a.insert('a', 0);
        a.insert('t', 1);
        a.insert('B', 0);
        a.insert('a', 0);
        a.insert('t', 1);
        System.out.println(a.size());
        System.out.println(a.toString());
        a.remove(0);
        System.out.println(a.toString());
        a.remove(0);
//        a.insert(null, 1);
//        System.out.println("index: " + a.indexOf(null));
//        System.out.println(a.toString());
//        a.remove(100);
        System.out.println(a.size());
        while (a.isEmpty() == false) {
            System.out.println(a.remove(0));
        }
        System.out.println(a.size());
//        c.insert(27, 27);
//        b.insert(27, 27);
//        System.out.println(c.equals(b));
//
    }

    //
    private static void queueTests() {
//        //todo: make more tests here
        Queue a = new Queue();
        Queue b = new Queue();
        Queue c = new Queue();
        c.enqueue(1);
        b.enqueue(1);
        a.enqueue('B');
        a.enqueue('a');
        a.enqueue('t');

        System.out.println(a.toString());

        while (a.isEmpty() == false) {
            System.out.println(a.dequeue());
        }
        a.dequeue();
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        a.enqueue(0);
        System.out.println(a.toString());
        System.out.println(a.equals(b));
        System.out.println(b.equals(c));
    }

    //
    private static void stackTests() {
        //todo: make more tests here
        Stack a = new Stack();
        a.pop();
        a.push('B');
        a.push('a');
        a.push('t');

        Stack b = new Stack();
        System.out.println(a.equals(b));

        b.push('B');
        b.push('a');
        b.push('t');
        System.out.println(a.equals(b));

        System.out.println(a.toString());
        a.pop();
        System.out.println(a.toString());
        a.push('t');
        System.out.println(a.toString());
        a.push(100);
        System.out.println(a.toString());
        while (a.isEmpty() == false) {
            System.out.println(a.pop());
        }

    }
}


